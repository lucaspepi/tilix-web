# Tilix Web Test

## Installation

To install dependencies run command:

```bash
npm install
```

## Setup

Command to run server:
```bash
npm run serve
```