import Vue from 'vue'
import Router from 'vue-router'
import List from './components/invoice/List'
import New from './components/invoice/New'
import Edit from './components/invoice/Edit'

Vue.use(Router)

export const constantRoutes = [
      {
        path: '/',
        name: 'list',
        component: List
      },
      {
        path: '/new',
        name: 'new',
        component: New
      },
      {
        path: '/edit/:id',
        name: 'edit',
        component: Edit
      }
]

export default new Router({
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes
})